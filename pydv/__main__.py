import argparse
import signal

import gpstime

from . import plot
from .version import __version__


description = """Plot time series of NDS channels.
"""


summary = description.splitlines()[0]
#usage = "dv [<options>] <channel> [<channel>...]"


def main():
    parser = argparse.ArgumentParser(description=description.strip(),
                                     formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('channel_names', metavar='channel', nargs='+', type=str, help='channel name')
    parser.add_argument('--debug', action='store_true', help='flag for very verbose output')
    parser.add_argument('--subplots', action='store_true', help='make a separate subplot for each channel')
    parser.add_argument('--sharex', action='store_true', help='share the x axis on all subplots')
    parser.add_argument('--sharey', action='store_true', help='share the y axis on all subplots')
    parser.add_argument('--time', default=None, help='end time for all channels (default: now)')
    parser.add_argument('--title', type=str, default=None, help='plot title')
    parser.add_argument('--lookback', type=float, default=10, help='time to look behind --time')
    parser.add_argument('--lookforward', type=float, default=0, help='time to look ahead of --time')
    parser.add_argument('--no-threshold', action='store_true', help='do not try to parse threshold channel names')
    parser.add_argument('--no-wd-state', action='store_true', help='do not try to parse watchdog state channel names')
    parser.add_argument('--version', action='version', version=__version__)
    args = parser.parse_args()

    if args.subplots:
        newChannelNames = []
        for channelName in args.channel_names:
            newChannelNames.append([channelName])
        args.channel_names = newChannelNames
    else:
        args.channel_names = [args.channel_names]

    time = gpstime.parse(args.time).gps()

    # for cleaner C-c handling
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    plot(
        channel_names=args.channel_names,
        xmin=time-args.lookback,
        xmax=time+args.lookforward,
        center_time=time,
        title=args.title,
        sharex=args.sharex,
        sharey=args.sharey,
        no_threshold=args.no_threshold,
        no_wd_state=args.no_wd_state,
        )


if __name__ == '__main__':
    main()
