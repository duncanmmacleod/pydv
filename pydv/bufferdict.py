import os
import numpy as np
from collections import namedtuple

import nds2

from . import util
from . import const


class NDSBuffer(object):
    class Exception(Exception):
        pass

    class SampleRateMismatch(Exception):
        pass

    class OverlapMismatch(Exception):
        pass

    logger = util.make_default_logger('NDSBuffer')

    def __init__(self, nds2buffer, is_wd_state=False, is_abs_threshold=False):
        self.__is_wd_state = is_wd_state
        self.__is_abs_threshold = is_abs_threshold
        self.channel_name = nds2buffer.channel.name
        self.sample_rate = int(nds2buffer.channel.sample_rate)
        self.length = nds2buffer.length
        start_time = nds2buffer.gps_seconds + 10 ** (-9) * float(nds2buffer.gps_nanoseconds)
        time_domain = np.arange(start_time, start_time + (len(nds2buffer.data)) * 1. / self.sample_rate,
                             1. / self.sample_rate)
        self.__internal_buffer = np.vstack([time_domain, np.array(nds2buffer.data)])

    def __str__(self):
        s = '< name: {0:{width}s} {{fs}} '.format(self.channel_name, width=const.MAX_CHANNEL_NAME_WIDTH) \
            + 'sample rate = {0:{width}.1f} {{fs}} '.format(self.sample_rate, width=const.MAX_SAMPLE_RATE_DIGITS) \
            + 'start time = {0:f} {{fs}} end time = {1:f} >'.format(self.start_time, self.end_time)
        s = s.format(fs='#')
        return s

    def get_data(self, start_time, end_time):
        assert (start_time < end_time)
        assert (start_time >= self.start_time)
        assert (end_time <= self.end_time)

        start_index = self.__time_to_index(start_time)
        end_index = self.__time_to_index(end_time)
        assert (self.__internal_buffer.shape[1] >= end_index - start_index)
        return self.__internal_buffer[1, start_index:end_index]

    def get_time_domain(self, start_time, end_time):
        start_index = self.__time_to_index(start_time)
        # TODO fix possible one-off error here
        end_index = self.__time_to_index(end_time)
        assert (self.__internal_buffer.shape[1] >= end_index - start_index)
        return self.__internal_buffer[0, start_index:end_index]

    def __time_to_index(self, time):
        return int(np.floor(self.sample_rate * (time - self.start_time)))

    @property
    def is_wd_state(self):
        return self.__is_wd_state

    @is_wd_state.setter
    def is_wd_state(self, b):
        assert (type(b) == bool)
        self.__is_wd_state = b

    @property
    def is_abs_threshold(self):
        return self.__is_abs_threshold

    @is_abs_threshold.setter
    def is_abs_threshold(self, b):
        assert (type(b) == bool)
        self.__is_abs_threshold = b

    @property
    def data(self):
        return self.__internal_buffer[1, :]

    @property
    def time_domain(self):
        return self.__internal_buffer[0, :]

    @property
    def start_time(self):
        return int(self.__internal_buffer[0, 0])

    @property
    def end_time(self):
        return int(self.__internal_buffer[0, -1] + 1. / self.sample_rate)

    def __add__(self, other):
        NDSBuffer.logger.debug('Combining two buffers with channel names:'
                               + '\n' + const.LOG_SECOND_LINE_INDENT * ' ' + self.channel_name
                               + '\n' + const.LOG_SECOND_LINE_INDENT * ' ' + other.channel_name)
        if self.channel_name != other.channel_name:
            NDSBuffer.logger.debug('The channels names do not match, but if the data is the same,'
                                + 'then everything should be okay.')
        if other.sample_rate != self.sample_rate:
            NDSBuffer.logger.debug('Cannot combine buffers with different sample rates.')
            NDSBuffer.logger.debug('Raising SampleRateMismatch.')
            raise NDSBuffer.SampleRateMismatch
        sample_rate = self.sample_rate
        if other.end_time < self.start_time or other.start_time > self.end_time:
            NDSBuffer.logger.debug('Cannot combine buffers whose overlapping entries do not match.')
            NDSBuffer.logger.debug('Raising OverlapMismatch.')
            raise NDSBuffer.OverlapMismatch

        # other.__internal_buffer[1,other_offset] == self.__internal_buffer[1,0]
        merged_start_time = min([self.start_time, other.start_time])
        merged_end_time = max([self.end_time, other.end_time])

        list_to_return = self
        list_to_merge = other
        merged_buffer = self.__internal_buffer

        class MergedBuffers(Exception):
            pass

        try:
            if other.start_time <= self.start_time and other.end_time >= self.end_time:
                merged_buffer = other.__internal_buffer
                raise MergedBuffers
            elif self.start_time <= other.start_time and self.end_time >= other.end_time:
                raise MergedBuffers

            merged_buffer = np.zeros((2, sample_rate * (merged_end_time - merged_start_time)))
            if other.start_time > self.start_time:
                list_to_return = other
                list_to_merge = self
            merged_buffer[:, :list_to_merge.__internal_buffer.shape[1]] = list_to_merge.__internal_buffer[:, :]
            offset = sample_rate * (list_to_merge.end_time - list_to_return.start_time)
            merged_buffer[:, list_to_merge.__internal_buffer.shape[1]:] = list_to_return.__internal_buffer[:, offset:]
            raise MergedBuffers
        except MergedBuffers:
            pass

        list_to_return.__internal_buffer = merged_buffer
        if list_to_return.is_wd_state or list_to_merge.is_wd_state:
            list_to_return.is_wd_state = True
        if list_to_return.is_abs_threshold or list_to_merge.is_abs_threshold:
            list_to_return.is_abs_threshold = True

        NDSBuffer.logger.debug('Successfully combined the buffers.')
        NDSBuffer.logger.debug('Returned buffer has channel name %s and contains data from %d to %d.'
                               % (list_to_return.channel_name, list_to_return.start_time, list_to_return.end_time))
        return list_to_return

    def __radd__(self, other):
        return self.__add__(other)


HostPortPair = namedtuple('HostPortPair', ['host', 'port'])


class NDSBufferDict(dict):
    class Exception(Exception):
        pass

    class NoNDSServerGiven(Exception):
        pass

    class ServerDownError(Exception):
        pass

    class ChannelRequestError(Exception):
        pass

    class FetchedChannelNameOrderMismatch(Exception):
        pass

    class BadBufferError(Exception):
        pass

    class StartTimeAfterEndTime(Exception):
        pass

    logger = util.make_default_logger('NDSBufferDict')

    def __init__(self, channel_names, start_time, end_time, wd_state_mask=None, abs_threshold_mask=None):
        super(NDSBufferDict, self).__init__()
        if start_time >= end_time:
            raise NDSBufferDict.StartTimeAfterEndTime
        NDSBufferDict.logger.debug('Established connection.')
        self.__start_time = start_time
        self.__end_time = end_time
        self.add_channels(channel_names, wd_state_mask=wd_state_mask, abs_threshold_mask=abs_threshold_mask)

    @property
    def start_time(self):
        return self.__start_time

    @start_time.setter
    def start_time(self, time):
        if time >= self.__end_time:
            raise NDSBufferDict.StartTimeAfterEndTime
        if time < self.__start_time:
            self.add_data(time, self.__start_time)

    @property
    def end_time(self):
        return self.__end_time

    @end_time.setter
    def end_time(self, time):
        if time <= self.__start_time:
            raise NDSBufferDict.StartTimeAfterEndTime
        if time > self.__end_time:
            self.add_data(self.__end_time, time)

    @property
    def channel_names(self):
        return list(self.keys())


    def check_connection(self):
        if 'conn' in self.__dict__.keys():
            return
        default_port = 31200
        sspec = os.getenv('NDSSERVER')
        if not sspec:
            raise self.NoNDSServerGiven("NDSSERVER environment variable not specified.")
        for hostport in sspec.split(','):
            try:
                host,port = hostport.split(':')
            except ValueError:
                host = hostport
                port = default_port
        self.conn = nds2.connection(host, int(port))

    def close_connection(self):
        del self.conn

    def refresh_connection(self):
        if 'conn' in self.__dict__.keys():
            self.close_connection()
        self.check_connection()


    def __fetch_data(self, t_start, t_end, channel_names, wd_state_mask=None, abs_threshold_mask=None):
        def check_mask(mask):
            if mask is None:
                mask = [False] * len(channel_names)
            else:
                assert (len(mask) == len(channel_names))
            return mask

        wd_state_mask = check_mask(wd_state_mask)
        abs_threshold_mask = check_mask(abs_threshold_mask)

        self.check_connection()

        fetched_channel_data = {}
        for i in range(2):
            try:
                # fetch requires integer arguments, so we use the
                # floor and ceiling of start_time and end_time respectively
                requested_t_start = int(np.floor(float(t_start)))
                requested_t_end = int(np.ceil(float(t_end)))
                msg = "Buffers requested at start time %d and end time %d for the following channels:" \
                      % (requested_t_start, requested_t_end)
                for name in channel_names:
                    msg += "\n" + const.LOG_SECOND_LINE_INDENT * ' ' + name
                NDSBufferDict.logger.info(msg)

                # we have to request at most six channels at a time because of a bug in nds
                for j in range(int(np.ceil(len(channel_names) / float(const.NDS_REQUEST_LIMIT)))):
                    six_channel_names = channel_names[const.NDS_REQUEST_LIMIT * j:
                                                      const.NDS_REQUEST_LIMIT * j + const.NDS_REQUEST_LIMIT]

                    msg = "Fetching buffers for the following channels from %d to %d:" \
                          % (requested_t_start, requested_t_end)
                    for channel_name in six_channel_names:
                        msg += '\n' + const.LOG_SECOND_LINE_INDENT * ' ' + channel_name
                    NDSBufferDict.logger.debug(msg)

                    retrieved = \
                        self.conn.fetch(requested_t_start,
                                        requested_t_end, six_channel_names)

                    NDSBufferDict.logger.debug("Buffers retreived.")

                    # check that all of the buffers
                    for k, ndsbuffer in enumerate(retrieved):
                        mask_idx = const.NDS_REQUEST_LIMIT * j + k
                        pyndsbuffer = NDSBuffer(ndsbuffer, is_wd_state=wd_state_mask[mask_idx],
                                                is_abs_threshold=abs_threshold_mask[mask_idx])
                        NDSBufferDict.logger.debug("Received %s" % pyndsbuffer)

                        if requested_t_start < pyndsbuffer.start_time:
                            NDSBufferDict.logger.warning("Requested buffer start time at %d" % requested_t_start
                                                         + " but received a buffer with start time %d"
                                                         % pyndsbuffer.start_time)
                            #raise BufferDict.BadBufferError
                        if requested_t_end > pyndsbuffer.end_time:
                            NDSBufferDict.logger.warning("Requested buffer end time at %d" % requested_t_end
                                                         + " but received a buffer with end time %d"
                                                         % pyndsbuffer.end_time)
                            #raise BufferDict.BadBufferError

                        fetched_channel_data[pyndsbuffer.channel_name] = pyndsbuffer  # append Buffer object

                break
            except RuntimeError:
                if i == 1:
                    error_msg = "NDS server cannot retrieve data for at least one of the following channels:"
                    for chan in channel_names:
                        error_msg += '\n'+const.LOG_SECOND_LINE_INDENT*' '+chan
                    NDSBufferDict.logger.critical(error_msg)
                    raise NDSBufferDict.ChannelRequestError(error_msg)
                NDSBufferDict.logger.warning('Trying to recover from error by clearing nds2 cache...')
                self.refresh_connection()

        msg = 'Summary of time series buffers received:'
        for channel_name in fetched_channel_data.keys():
            msg += '\n' + const.LOG_SECOND_LINE_INDENT * ' ' + channel_name
        NDSBufferDict.logger.debug(msg)

        self.close_connection()

        return fetched_channel_data

    def add_data(self, t_start, t_end):
        def fetch(t_start, t_end):
            fetched_channel_data = self.__fetch_data(t_start, t_end, self.channel_names)
            for channel_name in self.channel_names:
                self[channel_name] += fetched_channel_data[channel_name]
        r = False
        if t_start < self.__start_time:
            fetch(t_start, self.__start_time)
            self.__start_time = t_start
            r = True
        if t_end > self.__end_time:
            fetch(self.__end_time, t_end)
            self.__end_time = t_end
            r = True
        return r

    def add_channels(self, channel_names, wd_state_mask=None, abs_threshold_mask=None):
        assert (type(channel_names) == list)
        if wd_state_mask is not None:
            assert (len(wd_state_mask) == len(channel_names))
        if abs_threshold_mask is not None:
            assert (len(abs_threshold_mask) == len(channel_names))

        channel_names_to_fetch = []
        num_removed = 0
        for i, channel_name in enumerate(channel_names):
            if channel_name not in self.channel_names:
                channel_names_to_fetch.append(channel_name)
            else:
                if wd_state_mask is not None:
                    wd_state_mask.pop(i - num_removed)
                if abs_threshold_mask is not None:
                    abs_threshold_mask.pop(i - num_removed)
                num_removed += 1
        if not channel_names_to_fetch:
            return
        fetched_channel_data = self.__fetch_data(self.__start_time, self.__end_time, channel_names_to_fetch,
                                                 wd_state_mask=wd_state_mask,
                                                 abs_threshold_mask=abs_threshold_mask)
        for channel_name in channel_names_to_fetch:
            self[channel_name] = fetched_channel_data[channel_name]

    def add_wd_state_channel(self, channel_name):
        self.add_channels(list(channel_name), wd_state_mask=[True])

    @property
    def buffers(self):
        return list(self.values())
