import os
import sys
import unittest
import logging
import numpy as np

from gpstime import tconvert

from .bufferdict import NDSBufferDict


SECONDS_AGO_TO_TEST_AROUND = 6000
BUFFER_LENGTHS_IN_SECONDS = 10
MIN_TCONVERT_GPSTIME = 200000000

os.environ['NDSSERVER'] = 'nds.ligo-wa.caltech.edu'


def _add_stdout_logger(self, logger):
    formatter = logging.Formatter('        %(name)s - %(levelname)s - %(message)s')
    handler = logging.StreamHandler(stream=sys.stdout)
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)


class TestNDSBufferDict(unittest.TestCase):
    def setUp(self):
        #_add_stdout_logger(NDSBufferDict.logger)
        #_add_stdout_logger(NDSBuffer.logger)

        self.recent_time = float(tconvert('now'))-SECONDS_AGO_TO_TEST_AROUND
    
    @staticmethod
    def buffers_are_equal(c, d):
        if d.is_wd_state != c.is_wd_state:
            return False
        if d.channel_name != c.channel_name:
            return False
        if d.sample_rate != c.sample_rate:
            return False
        if d.start_time != c.start_time:
            return False
        if d.end_time != c.end_time:
            return False
        if not np.array_equal(d.data, c.data):
            return False
        if not np.array_equal(d.time_domain, c.time_domain):
            return False
        return True

    @staticmethod
    def buffer_dicts_are_equal(A, B):
        class Failed(Exception): pass
        try:
            if sorted(A.channel_names) != sorted(B.channel_names):
                raise Failed
            for channel_name in A.channel_names:
                if not TestNDSBufferDict.buffers_are_equal(A[channel_name], B[channel_name]):
                    raise Failed
        except Failed:
            return False
        return True

    def test_equality_method(self):
        A = NDSBufferDict(['H1:ISI-ITMX_ST1_CPSINF_H1_IN1_DQ'],
                          self.recent_time, self.recent_time+BUFFER_LENGTHS_IN_SECONDS)
        B = NDSBufferDict(['H1:ISI-ITMX_ST1_CPSINF_H1_IN1_DQ'],
                          self.recent_time, self.recent_time+BUFFER_LENGTHS_IN_SECONDS)
        self.assertTrue(TestNDSBufferDict.buffer_dicts_are_equal(A, B))

    def _get_recent_buffer(self, start, end):
        assert(end <= SECONDS_AGO_TO_TEST_AROUND)
        return NDSBufferDict(['H1:ISI-ITMX_ST1_CPSINF_H1_IN1_DQ'],
                             self.recent_time+start, self.recent_time+end).buffers[0]

    def test_buffer_merge_disjoint(self):
        B = dict()
        for args in [(0,BUFFER_LENGTHS_IN_SECONDS-5), (BUFFER_LENGTHS_IN_SECONDS-5,BUFFER_LENGTHS_IN_SECONDS), (0,BUFFER_LENGTHS_IN_SECONDS)]:
            B[args] = self._get_recent_buffer(*args)
        self.assertTrue(TestNDSBufferDict.buffers_are_equal(B[(0,BUFFER_LENGTHS_IN_SECONDS-5)]+B[(BUFFER_LENGTHS_IN_SECONDS-5,BUFFER_LENGTHS_IN_SECONDS)], B[(0,BUFFER_LENGTHS_IN_SECONDS)]))

    def test_buffer_merge_overlapping_left(self):
        B = dict()
        for args in [(0,BUFFER_LENGTHS_IN_SECONDS-3), (BUFFER_LENGTHS_IN_SECONDS-7,BUFFER_LENGTHS_IN_SECONDS), (0,BUFFER_LENGTHS_IN_SECONDS)]:
            B[args] = self._get_recent_buffer(*args)
        self.assertTrue(TestNDSBufferDict.buffers_are_equal(B[(0,BUFFER_LENGTHS_IN_SECONDS-3)]+B[(BUFFER_LENGTHS_IN_SECONDS-7,BUFFER_LENGTHS_IN_SECONDS)], B[(0,BUFFER_LENGTHS_IN_SECONDS)]))

    def test_buffer_merge_overlapping_right(self):
        B = dict()
        for args in [(0,BUFFER_LENGTHS_IN_SECONDS-3), (BUFFER_LENGTHS_IN_SECONDS-7,BUFFER_LENGTHS_IN_SECONDS), (0,BUFFER_LENGTHS_IN_SECONDS)]:
            B[args] = self._get_recent_buffer(*args)
        self.assertTrue(TestNDSBufferDict.buffers_are_equal(B[(BUFFER_LENGTHS_IN_SECONDS-7,BUFFER_LENGTHS_IN_SECONDS)]+B[(0,BUFFER_LENGTHS_IN_SECONDS-3)], B[(0,BUFFER_LENGTHS_IN_SECONDS)]))

    def test_buffer_merge_superset(self):
        B = dict()
        for args in [(BUFFER_LENGTHS_IN_SECONDS-13,BUFFER_LENGTHS_IN_SECONDS-3), (0,BUFFER_LENGTHS_IN_SECONDS)]:
            B[args] = self._get_recent_buffer(*args)
        self.assertTrue(TestNDSBufferDict.buffers_are_equal(B[(BUFFER_LENGTHS_IN_SECONDS-13,BUFFER_LENGTHS_IN_SECONDS-3)]+B[(0,BUFFER_LENGTHS_IN_SECONDS)], B[(0,BUFFER_LENGTHS_IN_SECONDS)]))

    def test_buffer_dict_lots_of_channels_equality(self):
        A = NDSBufferDict(['H1:ISI-ITMX_ST1_CPSINF_H1_IN1_DQ',
                           'H1:ISI-ITMX_ST1_CPSINF_H2_IN1_DQ',
                           'H1:ISI-ITMX_ST1_CPSINF_H3_IN1_DQ',
                           'H1:ISI-ITMX_ST1_CPSINF_V1_IN1_DQ',
                           'H1:ISI-ITMX_ST1_CPSINF_V2_IN1_DQ',
                           'H1:ISI-ITMX_ST1_CPSINF_V3_IN1_DQ'],
                          self.recent_time, self.recent_time+BUFFER_LENGTHS_IN_SECONDS-5)
        A.add_data(self.recent_time+BUFFER_LENGTHS_IN_SECONDS-5, self.recent_time+BUFFER_LENGTHS_IN_SECONDS)

        B = NDSBufferDict(['H1:ISI-ITMX_ST1_CPSINF_H1_IN1_DQ',
                           'H1:ISI-ITMX_ST1_CPSINF_H2_IN1_DQ',
                           'H1:ISI-ITMX_ST1_CPSINF_H3_IN1_DQ',
                           'H1:ISI-ITMX_ST1_CPSINF_V1_IN1_DQ',
                           'H1:ISI-ITMX_ST1_CPSINF_V2_IN1_DQ',
                           'H1:ISI-ITMX_ST1_CPSINF_V3_IN1_DQ'],
                          self.recent_time, self.recent_time+BUFFER_LENGTHS_IN_SECONDS)
        self.assertTrue(TestNDSBufferDict.buffer_dicts_are_equal(A, B))
        

def instantiate_test_cases(test_case):
    import re
    return map(test_case, filter(lambda method_name: re.match('^test_.*', method_name), dir(test_case)))


test_all = unittest.TestSuite()
test_all.addTests(instantiate_test_cases(TestNDSBufferDict))
# test_all.addTests(instantiate_test_cases(TestGPSTime))


if __name__ == '__main__':
    unittest.main()


#
## NDS is buggy if I request more than 6 channels!!!
#print 'Testing NDS2BufferList add_channels method'
#buffer_list8.add_channels(['H1:ISI-ITMX_ST2_CPSINF_V3_IN1_DQ'])
#buffer_list9 = NDSBufferDict(['H1:ISI-ITMX_ST1_CPSINF_H1_IN1_DQ',
#                              'H1:ISI-ITMX_ST1_CPSINF_H2_IN1_DQ',
#                              'H1:ISI-ITMX_ST1_CPSINF_H3_IN1_DQ',
#                              'H1:ISI-ITMX_ST1_CPSINF_V1_IN1_DQ',
#                              'H1:ISI-ITMX_ST1_CPSINF_V2_IN1_DQ',
#                              'H1:ISI-ITMX_ST1_CPSINF_V3_IN1_DQ',
#                              'H1:ISI-ITMX_ST2_CPSINF_V3_IN1_DQ'],
#                             1066827000, 1066828200)
#buffer_lists_are_equal(buffer_list8, buffer_list9)
#
#print 'Testing labeling of added WD state channel'
#buffer_list8.add_wd_state_channel(['H1:ISI-ITMX_ST2_WDMON_STATE_INMON'])
#print 'ok' if buffer_list8[7].is_wd_state else 'failed'
#
#print "Testing ability to retrieve a channel from LHO"
#buffer_list1 = NDSBufferDict(['H1:ISI-ITMX_ST1_CPSINF_H1_IN1_DQ'],
#                             1066828000, 1066828010, host='nds.ligo-wa.caltech.edu', port=31200)
#buffer_list2 = NDSBufferDict(['H1:ISI-ITMX_ST1_CPSINF_H1_IN1_DQ'],
#                             1066828000, 1066828010, host='nds.ligo-wa.caltech.edu', port=31200)
#if np.array_equal(buffer_list1[0].data, buffer_list2[0].data):
#    print 'ok'
#else:
#    print 'failed'
#
