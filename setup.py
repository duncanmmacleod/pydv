# much of the structure here was cribbed from
# https://github.com/pypa/sampleproject

from setuptools import setup


# Get the long description from the README file
with open('README.md', encoding='utf-8') as f:
    long_description = f.read()


version = {}
with open("pydv/version.py") as f:
    exec(f.read(), version)


setup(
    name = 'pydv',
    version = version['__version__'],
    description = 'NDS timeseries plotting utility.',
    long_description = long_description,
    author = 'Charles Celerier',
    author_email = 'charles.celerier@ligo.org',
    url = 'https://git.ligo.org/cds/pydv',
    license = 'GPLv3+',
    packages = ['pydv'],
    keywords = ['nds', 'nds2', 'plotting'],
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Programming Language :: Python :: 2 :: Only',
        'Operating System :: POSIX',
        # FIXME: also works to produce plots from command line
        'Environment :: X11 Applications',
        ],
    # install_requires = [
    #     'matplotlib',
    #     'nds2',
    #     'gpstime',
    #     ],
    # You can install this optional dependency using the following syntax:
    # $ pip install -e .xdo
    # extras_require={
    #     'xdo': ['xdo'],
    # },
    # https://chriswarrick.com/blog/2014/09/15/python-apps-the-right-way-entry_points-and-scripts/
    # should we have a 'gui_scripts' as well?
    entry_points={
        'console_scripts': [
            'pydv = pydv.__main__:main',
        ],
    },
)
